import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import InstaLogo from './../icons/insta.png'
import FacebookLogo from './../icons/facebook.png'
import SnapLogo from './../icons/snap.png'
import TwitterLogo from './../icons/twitter_blue.png'

import AccountCircle from '@material-ui/icons/AccountCircle';
const socialMedia = [
    {
        name: 'Facebook',
        color: '#3B5998',
        src: FacebookLogo,
        url: 'https://facebook.com/'
    },
    {
        name: 'Snapchat',
        src: SnapLogo,
        // color: '#fffd5b',
        color: '#FFFC00',
        url: ''
    },
    {
        name: 'Twitter',
        src: TwitterLogo,
        color: '#1DA1F2',
        url: ''
    },
    {
        name: 'Instagram',
        src: InstaLogo,
        color: '#8a3ab9',
        url: ''
    }
]


export default class CreateContact extends React.Component {
    constructor() {
        super();
        this.state = {
            givenName: "",
            familyName: "",
            organizationName: "",
            imageData: "",
            socialProfiles: [],
            urls: [],
            postalAddresses: [],
            emails: [],
            numOfEmails: 1,
        }
    }

    renderEmails() {
        const emailComponent = [];
        for (let i = 0; i < this.state.numOfEmails; i++) {
            let email;
            let label = `Email ${i + 1}`
            emailComponent.push(
                <TextField
                    id="Email"
                    label={label}
                    value={email}
                    onChange={this.handleChangeEmails}
                    margin="normal"
                    name={i}
                    fullWidth
                />
            )
        }
        return emailComponent;

    }

    renderSocialMedia() {
        let socialProfilesState = []
       

        let socialMediaComponent = [];
        for (let i = 0; i < socialMedia.length; i++) {
            let elem = socialMedia[i]

            let theme = createMuiTheme({
                palette: {
                    primary: {
                        light: elem.color,
                        main: elem.color,
                        dark: elem.color,
                    }
                },
            });
            let value;
            socialMediaComponent.push(

                <MuiThemeProvider theme={theme}>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item >
                            <Avatar alt='social media' src={elem.src} />
                        </Grid>
                        <Grid item >
                            <TextField
                                id="SM"
                                name={i}
                                label={elem.name}
                                value={value}
                                margin="normal"
                                onChange={this.handleChangeSocial}
                                fullWidth />
                        </Grid>

                    </Grid>
                </MuiThemeProvider>
            )
        }


        return (socialMediaComponent)
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(() => ({
            [name]: value
        }))
    }

    addEmail = () => {
        this.setState(prevState => ({
            numOfEmails: ++prevState.numOfEmails
        }));
    }

    handleChangeSocial = e => {
    
        const { name, value,label } = e.target;
        console.log(name, value)
        let index = parseInt(name)
        const socialProfiles = this.state.socialProfiles;
        socialProfiles[index] = {
            username: value,
            service: socialMedia[index].name
        };
        // update state
        this.setState({
            socialProfiles,
        });
        // this.setState({ emails: [...this.state.emails, value] }) //simple value
    }

    handleChangeEmails = e => {
        const { name, value } = e.target;
        let index = parseInt(name)
        const emails = this.state.emails;
        emails[index] = {
            mail: value
        };
        // update state
        this.setState({
            emails,
        });
        // this.setState({ emails: [...this.state.emails, value] }) //simple value
    }

    generateContact = () => {
        let contact = {
            typeFormat: "com.mydotcard.contact.0.0.2",
            contact: {
                contactType: "person/organzation",
                givenName: this.state.givenName,
                familyName: this.state.familyName,
                socialProfiles: this.state.socialProfiles,
                emails:this.state.emails
            }
        };
        console.log(contact)
        alert(contact)

    }



    render() {
        console.log(this.state)
        const { givenName, familyName, organizationName, imageData, socialProfiles, urls, postalAddresses, emails } = this.state;
        let testEmail = ""
        return (

            <div style={{ padding: '40px' }}>
                <Grid container spacing={40}>
                    <Grid item xs={6}>
                        <Grid container spacing={16} justify={'center'} direction={'column'}>
                            <Grid item xs={12}>
                                <TextField
                                    id="givenName"
                                    label="First Name"
                                    name="givenName"
                                    value={givenName}
                                    onChange={this.handleChange}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="familyName"
                                    label="Last Name"
                                    name="familyName"
                                    value={familyName}
                                    onChange={this.handleChange}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                {this.renderEmails()}
                                <Button variant="fab"
                                    color="primary"
                                    aria-label="Add"
                                    onClick={this.addEmail}
                                >
                                    <AddIcon />
                                </Button>
                            </Grid>


                        </Grid>
                    </Grid>
                    <Grid item xs={1}>
                    </Grid>

                    <Grid item xs={5}>
                        <Grid container spacing={16} justify={'center'} direction={'column'}>
                            <Grid item xs={12}>
                                {this.renderSocialMedia()}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Button variant="contained" color="primary" onClick={this.generateContact}
                >
                    Log Contact
                 </Button>
            </div>
        );
    }
}


